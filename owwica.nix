{ mkDerivation, base, base-prelude, bytestring, cmdargs, directory
, filepath, hledger-lib, hspec, hspec-discover, HUnit, interpolate
, io-streams, mockery, QuickCheck, regex-posix, split, stdenv
, temporary, text, zlib
}:
mkDerivation {
  pname = "owwica";
  version = "0.0.23";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base base-prelude bytestring directory filepath hledger-lib hspec
    io-streams QuickCheck regex-posix split text zlib
  ];
  libraryToolDepends = [ hspec-discover ];
  executableHaskellDepends = [
    base base-prelude bytestring cmdargs directory filepath hledger-lib
    hspec io-streams QuickCheck regex-posix split text zlib
  ];
  executableToolDepends = [ hspec-discover ];
  testHaskellDepends = [
    base base-prelude bytestring directory filepath hledger-lib hspec
    HUnit interpolate io-streams mockery QuickCheck regex-posix split
    temporary text zlib
  ];
  testToolDepends = [ hspec-discover ];
  homepage = "https://github.com/agander/owwica#readme";
  license = stdenv.lib.licenses.gpl2;
}

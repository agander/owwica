{
  nixpkgs ? import <nixpkgs> {}, compiler ? "ghc"
}:

nixpkgs.pkgs.haskell.packages.${compiler}.callPackage ./owwica.nix { }


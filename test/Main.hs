import Test.Tasty
import Test.Tasty.HUnit
import Lib

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ testCase "2+2=4" $
      2+2 @?= 4
  --, testCase "7 is even" $
      --assertBool "Oops, 7 is odd" (even 7)
  , testCase "filter doesDirectoryExist'" $
      filter doesDirectoryExist' ["./dist-newstyle", "./m4", "./knobby_knob_knobbs"] @?=
        ["./dist-newstyle", "./m4"]

  ]

